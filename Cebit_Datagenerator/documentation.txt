

### Data Generator - API

Base URL: https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com
path: /sensorData

parameters:
* attractionsNumber - number of attractions in the fun-park (default 19)
* numOfDays - number of days when sensors data was gathered (default 30)
* openingHour - the hour when the fun-park openes each day (default 10)
* startYear - year of the sensors data collection start (default 2018)
* startMonth - months of the sensors data collection start (default 5)
* startDay - day of the sensors data collection start (default 4)
* parkOpeningMinutes - number of minutes when the fun-park is opened (default 480)
* sensorsMinuteStep - step/gap in minutes between two consequtive sensors measurments (defualt 5)

Examples:
```
curl https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com/sensorData -o queue_sizes.csv

curl https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com/sensorData?numOfDays=2 -o queue_sizes.csv
```

### Time series forecast - API

Base URL: https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com
path: /forecast

parameters:
* attractionsNumber - number of attractions in the fun-park (default 1)
* trainingWindowSize - window size for time delay ML estimator. It determines the number of historical time steps taken into single iteration of training (default 96)
* parkOpeningMinutes - number of minutes when the fun-park is opened (default 480)
* sensorsMinuteStep - step/gap in minutes between two consequtive sensors measurments (defualt 5)


##### Asynchronous communication, as the job runs more than 10 minutes.

```
curl -X POST https://ml-cebit-01-timeseries.cfapps.eu10.hana.ondemand.com/forecastTrigger -F 'file=@queue_sizes.csv' -F "attractionsNumber=19"
```

copy jobId, for example: `89b70404-e248-4cdd-aaab-fff64e2776ce` and query for the result. The result is returned once the job finishes, otherwise you get 'Job Pending'.

```
curl -X GET https://ml-cebit-01-timeseries.cfapps.eu10.hana.ondemand.com/forecastGetResult?jobId=89b70404-e248-4cdd-aaab-fff64e2776ce -o waiting_rewards.csv
```


### Travelling salesman problem - API








