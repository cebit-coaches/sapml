####################################################################################################
#'''
# Created on 25.05.2018 initially
# data generator for Amusement Park for Cebit
# @author: Marco Ertel, Marek Polewczyk
#'''
####################################################################################################

import sys
import time
import math
import random
import gaus2
import socket
import datetime
from pandas import DataFrame
import pandas as pd
from matplotlib import pyplot

class QueuePusher:

    OPENING_HOUR = 10
    NUM_OF_DAYS = 30
    # FILE_NAME = 'queue_sizes.csv'

    START_YEAR = 2018
    START_MONTH = 5
    START_DAY = 4

    ATTRACTIONS_NUMBER = 19
    PARK_OPENING_MINUTES_NUM = 480
    MINUTES_STEP = 5

    def __init__(self, attractions_number, num_of_days, opening_hour, start_year,
                    start_month, start_day, park_opening_minutes, sensors_minutes_step):
        self.ATTRACTIONS_NUMBER = attractions_number
        self.NUM_OF_DAYS = num_of_days
        self.OPENING_HOUR = opening_hour
        self.START_YEAR = start_year
        self.START_MONTH = start_month
        self.START_DAY = start_day
        self.PARK_OPENING_MINUTES_NUM = park_opening_minutes
        self.MINUTES_STEP = sensors_minutes_step

    def queue_pusher(self, filename):
        dataframe_concat = DataFrame()

        # we start on 4th of may - Friday
        startDate = datetime.datetime(self.START_YEAR, self.START_MONTH, self.START_DAY, hour=self.OPENING_HOUR, minute=0, second=0, microsecond=0)
        nextTime = startDate

        # set the datetimes
        for day in range(0, self.NUM_OF_DAYS):
            startDate = startDate + datetime.timedelta(days = 1)
            startDate = startDate.replace(hour=self.OPENING_HOUR, minute=0, second=0, microsecond=0)
            nextTime = startDate

            is_holidays = 0
            # check if it is a weekend
            weekno = startDate.weekday()
            if weekno >= 5:
                is_holidays = 1

            for i in range(0, int(self.PARK_OPENING_MINUTES_NUM / self.MINUTES_STEP)): # minutes, opening time park
                nextTime = nextTime + datetime.timedelta(minutes = self.MINUTES_STEP)
                dataframe_concat = dataframe_concat.append({'datetime': nextTime}, ignore_index=True)


        for i2 in range (1, self.ATTRACTIONS_NUMBER + 1):
            startDate = datetime.datetime(self.START_YEAR, self.START_MONTH, self.START_DAY, hour=10, minute=0, second=0, microsecond=0) # park is opened between 10-18
            nextTime = startDate
            dataframe = DataFrame()
            dataframe_x = DataFrame()

            for day in range(0, self.NUM_OF_DAYS):
                is_holidays = False
                # check if it is a weekend
                weekno = startDate.weekday()
                if weekno >= 5:
                    is_holidays = True

                startDate = startDate + datetime.timedelta(days = 1)
                startDate = startDate.replace(hour=self.OPENING_HOUR, minute=0, second=0, microsecond=0)
                nextTime = startDate

                for i in range (0, int(self.PARK_OPENING_MINUTES_NUM / self.MINUTES_STEP)): # minutes, opening time park
                    nextTime = nextTime + datetime.timedelta(minutes = self.MINUTES_STEP)
                    if is_holidays:
                        # x1.5 users more to each attraction on holidays
                        queueLength = gaus2.waitingPersons(nextTime, i2, 1.5)
                    else:
                        queueLength = gaus2.waitingPersons(nextTime, i2)

                    # functional = gaus2.isAttractionFunctional(i2)
                    dataframe = dataframe.append({('a'+str(i2)+'-queue-size'): queueLength}, ignore_index=True)

            print("attraction: " + str(i2))
            dataframe_concat = pd.concat([dataframe_concat, dataframe], axis=1)

        dataframe_concat.set_index('datetime', inplace=True)
        # dataframe_concat.plot()
        # pyplot.show()
        dataframe_concat.to_csv(filename)

