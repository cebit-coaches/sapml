from flask import Flask
from flask import send_from_directory
from flask import send_file
from flask import request
import os
from queue_pusher import QueuePusher
import uuid

app = Flask(__name__)
static_file_dir = os.path.dirname(os.path.realpath(__file__))

# Get port from environment variable or choose 9099 as local default
port = int(os.getenv("PORT", 8080))

@app.route('/sensorData')
def sendFile():
	opening_hour = int(request.args.get('openingHour', 10))
	num_of_days = int(request.args.get('numOfDays', 30))
	attractions_number = int(request.args.get('attractionsNumber', 19))

	start_year = int(request.args.get('startYear', 2018))
	start_month = int(request.args.get('startMonth', 5))
	start_day = int(request.args.get('startDay', 4))

	park_opening_minutes = int(request.args.get('parkOpeningMinutes', 480))
	sensors_minutes_step = int(request.args.get('sensorsMinuteStep', 5))

	filename = str(uuid.uuid4()) + '.csv'
	queue_pusher = QueuePusher(attractions_number, 
								num_of_days, 
								opening_hour,
								start_year,
								start_month,
								start_day,
								park_opening_minutes,
								sensors_minutes_step)
	queue_pusher.queue_pusher(filename)

	return send_file(static_file_dir + '/' + filename,
						mimetype='text/csv',
						attachment_filename='queue_sizes.csv',
						as_attachment=True)


if __name__ == '__main__':
	# Run the app, listening on all IPs with our chosen port number
	app.run(host='0.0.0.0', port=port)