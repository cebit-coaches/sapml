from scipy.stats import norm
from random import randint
import datetime
import sys

def normpdf(x, mu=0, sigma=1):
    u = float((x-mu) / abs(sigma))
    y = exp(-u*u/2) / (sqrt(2*pi) * abs(sigma))
    return y

def waitingPersons(dtime:datetime, attr:int, holidays_usage_increase:int = 1.0)-> int:
    pers = 1
    distribution_mean_shift = getDistMeanShift(attr)
    maxpers = getMaxPers(attr)
    #calculate from dtime to value between 0 and 480
    
    calcMinute = (dtime - datetime.timedelta(hours=10)).minute
    calcHour = (dtime - datetime.timedelta(hours=10)).hour

    calcTime = calcMinute + (60 * calcHour)
    if calcTime > 480:
        calcTime = 0
    # print ("calcTime: " + str(calcTime))
    
    # check if distribution_mean_shift in range 0-480 and calculate factors
    if distribution_mean_shift > 0 and distribution_mean_shift < 480:
        if calcTime > distribution_mean_shift:
            calcTime = calcTime - distribution_mean_shift
        else:
            calcTime = 0

    #calculate actual length of queue
    x = ((calcTime / 480) * 2.9) - 1.45
    #print("x:"+str(x))
    factor = round((randint(0, 100) / 1000) - 0.05, 2)
    #print("factor:"+str(factor))
    if calcTime < 240:
        shift = round((randint(0, maxpers / 5) - (maxpers / 20)) * (calcTime / 240))
    else:
        shift = round((randint(0, maxpers / 5) - (maxpers / 20)) * ((480 - calcTime) / 480))
    #print("shift:" + str(shift))
    mean = 0
    standard_deviation = getDistStdDev(attr)
    pers = round(abs((norm.pdf(x, mean, standard_deviation) * 1.0 / 
                    norm.pdf(norm.mean(mean, standard_deviation), mean, standard_deviation) * maxpers) * 
                    (1 + factor) + shift)) * holidays_usage_increase

    # make the distribution zero on the edges
    if (calcTime) < 1 or (calcTime + distribution_mean_shift) > 475:
        pers = 0
    return int(pers)


def getDistStdDev(attr:int):
    return {
        1 : 0.698,
        2 : 0.398,
        3 : 0.450,
        4 : 0.398,
        5 : 0.200,
        6 : 0.398,
        7 : 0.598,
        8 : 0.598,
        9 : 0.398,
        10 : 0.210,
        11 : 0.398,
        12: 0.198,
        13 : 0.120,
        14: 0.238,
        15: 0.398,
        16: 0.698,
        17: 0.198,
        18: 0.398,
        19 : 0.100
    }.get(attr, 0)   

# walking distance from the entrance
def getDistMeanShift(attr:int):
    return {
        1 : 30,
        2 : 50,
        3 : 78,
        4 : 51,
        5 : 89,
        6 : 62,
        7 : 35,
        8 : 40,
        9 : 54,
        10 : 66,
        11 : 106,
        12: 83,
        13 : 72,
        14: 51,
        15: 47,
        16: 31,
        17: 110,
        18: 48,
        19 : 90
    }.get(attr, 0)

def getMaxPers(attr:int):
    return {
        1 : 300,
        2 : 200,
        3 : 250,
        4 : 200,
        5 : 100,
        6 : 250,
        7 : 300,
        8 : 300,
        9 : 200,
        10 : 150,
        11 : 200,
        12: 100,
        13 : 100,
        14: 150,
        15: 250,
        16: 400,
        17: 100,
        18: 200,
        19 : 50
    }.get(attr, 0)

def isAttractionFunctional(attr:int):
    return True

