from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
from pandas.tools.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from pandas import DataFrame
from sklearn.metrics import mean_squared_error
from pandas import Series
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import RFE
import sys
import numpy as np
from statsmodels.tsa.stattools import acf, pacf

MAKE_SERIES_STATIONARY_DIFF = False
MAKE_SERIES_STATIONARY_LOG = False
PREDICT_ONE_AHEAD = False

TRAINING_WINDOW_SIZE = 24
FILE_NAME = 'queue_sizes.csv'


class ArimaForestForecast(object):

	def __init__(self, attraction_number: int):
		self.attraction_number = attraction_number
		data_frame = DataFrame.from_csv('Cebit_Datagenerator/' + FILE_NAME, header=0) # 'car-sales.csv'
		self.series_orig = data_frame.ix[:, attraction_number]
		self.series = self.series_orig

		if MAKE_SERIES_STATIONARY_LOG:
			self.series = np.log(self.series)
			self.series_orig = self.series

		self.data_set_init = self.series

		self.data_set = self.series.values
		# to use the same data as in random forest
		self.data_set = self.data_set[TRAINING_WINDOW_SIZE:]


	def train(self):
		print("trainning started for attraction: " + str(self.attraction_number))
		
		data_set = self.series.values
		data_set = self.data_set[TRAINING_WINDOW_SIZE:]

		# split into training and test set
		self.training_set_size = int(len(data_set) * 0.7)
		self.test_set_size = len(data_set) - self.training_set_size

		train, self.test = data_set[0:self.training_set_size], data_set[self.training_set_size:len(data_set)]
		history = [x for x in train]

		if MAKE_SERIES_STATIONARY_DIFF:
			self.model = ARIMA(history, (20, 1, 0))
		else:
			self.model = ARIMA(history, (20, 0, 0))

		self.model = self.model.fit(disp=0, transparams=True)
		print("trainning finished for attraction: " + str(self.attraction_number))


	def predict_look_ahead(self, prediction_no):
		self.prediction_no = prediction_no
		output = self.model.forecast(prediction_no)
		self.predictions = output[0]

	def display_results(self):
		self.test = np.array(self.test)
		self.predictions = np.array(self.predictions)

		if MAKE_SERIES_STATIONARY_LOG:
			self.test = np.exp(self.test)
			self.predictions = np.exp(self.predictions)
			self.data_set_init = np.exp(self.data_set_init)

		error = mean_squared_error(self.test[:self.prediction_no], self.predictions)
		print('Test MSE: %.3f' % error)

		self.data_set_init = self.data_set_init[TRAINING_WINDOW_SIZE:]

		pyplot.plot(self.data_set_init[0 : self.training_set_size + 1], color='green')
		pyplot.plot(self.data_set_init[self.training_set_size : len(self.data_set_init.values)], color='blue')
		self.predictions = Series(data=self.predictions, index=self.data_set_init[self.training_set_size : + self.training_set_size + self.prediction_no].index)
		pyplot.plot(self.predictions, color='red')
		pyplot.show()


if __name__ == '__main__':
	estimator = ArimaForestForecast(1)
	estimator.train()
	estimator.predict(estimator.test_set_size)
	estimator.display_results()