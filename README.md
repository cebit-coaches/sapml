# Hackathon

You can find the full documentation with results in `hackathon.ipynb`

# API documentation

Please, find below three webservices necessary to solve the problem of finding a path in the fun-park. Each team has own webservices and the url for each team has to be adjusted accordingly by filling <SPACE_NUMBER>.

You can also modify the code of webservices as you wish. To do that adjust the code, go to the folder of the desired webservice (Cebit_Datagenerator, Cebit_Timeseries_Forecast, Cebit_TSP), fill the name of the webservice in the `manifest.yml` file (especially adjust <SPACE_NUMBER>) and run the command: `cf push`

To see the logs of the webservice, run the command: 

* `cf logs ml-cebit-<SPACE_NUMBER>-tsp --recent`

* `cf logs ml-cebit-<SPACE_NUMBER>-timeseries --recent`

* `cf logs ml-cebit-<SPACE_NUMBER>-data-generator --recent`

### Data Generator - API

`Base URL`: https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com

`path`: /sensorData `GET`


`parameters`:

* `attractionsNumber` - number of attractions in the fun-park (default 19)
 
* `numOfDays` - number of days when sensors data was gathered (default 30)

* `openingHour` - the hour when the fun-park openes each day (default 10)

* `startYear` - year of the sensors data collection start (default 2018)

* `startMonth` - months of the sensors data collection start (default 5)

* `startDay` - day of the sensors data collection start (default 4)

* `parkOpeningMinutes` - number of minutes when the fun-park is opened (default 480)

* `sensorsMinuteStep` - step/gap in minutes between two consequtive sensors measurments (defualt 5)



```console
curl https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com/sensorData -o queue_sizes.csv
```
```console
curl https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com/sensorData?numOfDays=2 -o queue_sizes.csv
```

### Time series forecast - API

Asynchronous communication, as the job runs more than 10 minutes

`Base URL`: https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com

`path`: /forecastTrigger `POST`


`parameters:`

* `attractionsNumber` - number of attractions in the fun-park (default 1)

* `trainingWindowSize` - window size for time delay ML estimator. It determines the number of historical time steps taken into single iteration of training (default 96)

* `parkOpeningMinutes` - number of minutes when the fun-park is opened (default 480)

* `sensorsMinuteStep` - step/gap in minutes between two consequtive sensors measurments (defualt 5)

* `training_set_end_date` - datetime when the training set ends (only points until this datetime will be included in training) (default '2018-05-26 10:05:00')

* `file` - file obtained from the https://ml-cebit-<SPACE_NUMBER>-data-generator.cfapps.eu10.hana.ondemand.com/sensorData endpoint


`Base URL`: https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com

`path`: /forecastGetResult `GET`


`parameters`:

* `jobId` - id of the job obtained from the /forecastGetResult endpoint


```console
curl -X POST https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com/forecastTrigger -F 'file=@queue_sizes.csv' -F "attractionsNumber=19"
```

copy jobId, for example: `89b70404-e248-4cdd-aaab-fff64e2776ce` and query for the result. The result is returned once the job finishes, otherwise you get 'Job Pending'.

```console
curl -X GET https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com/forecastGetResult?jobId=89b70404-e248-4cdd-aaab-fff64e2776ce -o waiting_rewards.json
```

### Travelling salesman problem - API

`Base URL`: https://ml-cebit-<SPACE_NUMBER>-tsp.cfapps.eu10.hana.ondemand.com

`path`: /find `POST`


`parameters:`

* `file_walking_rewards` - file containing walking times between each attraction (adjacency matrix)

* `file_waiting_rewards` - file obtained from the https://ml-cebit-<SPACE_NUMBER>-timeseries.cfapps.eu10.hana.ondemand.com/forecastGetResult endpoint

* `startState` - start state for the route planning (default 0 - fun-park entrance)

* `endState` - end state for the route planning (default 20 - fun-park exit)

```console
curl -X POST https://ml-cebit-<SPACE_NUMBER>-tsp.cfapps.eu10.hana.ondemand.com/find -F 'file_walking_rewards=@walking_rewards.csv' -F 'file_waiting_rewards=@waiting_rewards.json' -F "startState=0" -F "endState=20" -F "attractionsNumber=19"
```

