from flask import Flask
from flask import send_from_directory
from flask import send_file
from flask import request
import os
import uuid
from random_forest_forecast_class import RandomForestForecast
from flask import jsonify
from multiprocessing import Process, Value, Manager


app = Flask(__name__)
static_file_dir = os.path.dirname(os.path.realpath(__file__))

# Get port from environment variable or choose 9099 as local default
port = int(os.getenv("PORT", 8080))

results = Manager().dict()

class processClass:
	def __init__(self, job_id, filename, attractions_number, training_window_size, 
					park_opening_minutes, sensors_minutes_step, training_set_end_date='2018-05-26 10:05:00'):
		self.training_set_end_date = training_set_end_date
		self.attractions_number = attractions_number
		self.training_window_size = training_window_size
		self.park_opening_minutes = park_opening_minutes
		self.sensors_minutes_step = sensors_minutes_step
		self.filename = filename
		self.job_id = job_id
		p = Process(target=self.run, args=())
		p.daemon = True                       # Daemonize it
		p.start()                             # Start the execution

	def run(self):
		global results
		waiting_rewards = []

		for i in range(self.attractions_number):
			estimator = RandomForestForecast(self.filename, i, self.training_window_size)
			estimator.train(self.training_set_end_date)
			waiting_rewards.append(estimator.predict_look_ahead(int(self.park_opening_minutes/self.sensors_minutes_step))) 

		results[self.job_id] = waiting_rewards
		os.unlink(self.filename)


@app.route('/forecastTrigger', methods=['POST'])
def forecastTrigger():
	global results
	job_id = str(uuid.uuid4())
	results[job_id] = None
	filename = str(uuid.uuid4()) + '.csv'
	f = request.files['file']
	f.save(filename)

	attractions_number = int(request.form.get('attractionsNumber', 19))
	training_window_size = int(request.form.get('trainingWindowSize', 96))
	park_opening_minutes = int(request.form.get('parkOpeningMinutes', 480))
	sensors_minutes_step = int(request.form.get('sensorsMinuteStep', 5))
	training_set_end_date = request.form.get('training_set_end_date', '2018-05-26 10:05:00')

	try:
		processClass(job_id, filename, attractions_number, training_window_size, 
					park_opening_minutes, sensors_minutes_step, training_set_end_date)
	except:
		abort(500)

	return job_id


@app.route('/forecastGetResult', methods=['GET'])
def forecastGetResult():
	global results
	job_id = request.args.get('jobId')

	if results.get(job_id) != None:
		return jsonify(results[job_id]), 200
	else:
		return "Job Pending", 202


if __name__ == '__main__':
	# Run the app, listening on all IPs with our chosen port number
	app.run(host='0.0.0.0', port=port)