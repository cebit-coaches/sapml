from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
from pandas.tools.plotting import autocorrelation_plot
from pandas import DataFrame
from sklearn.metrics import mean_squared_error
from pandas import Series
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import RFE
import sys
import numpy as np

MAKE_SERIES_STATIONARY_DIFF = False
MAKE_SERIES_STATIONARY_LOG = False


class RandomForestForecast(object):

	def __init__(self, filename, attraction_number, training_window_size=96):
		self.TRAINING_WINDOW_SIZE = training_window_size
		self.attraction_number = attraction_number
		data_frame = DataFrame.from_csv(filename, header=0) # car-sales.csv
		self.series_orig = data_frame.ix[:, attraction_number]
		self.series = self.series_orig

		if MAKE_SERIES_STATIONARY_LOG:
			self.series = np.log(self.series)
			self.series_orig = self.series

		if MAKE_SERIES_STATIONARY_DIFF:
			self.init_state = self.series[0]
			self.series = self.series.diff()
			self.series = self.series[1:]

		self.data_set_init = self.series


	def train(self, training_set_end_date='2018-05-26 10:05:00'):
		print("trainning started for attraction: " + str(self.attraction_number))
		dataframe = DataFrame()
		for i in range(self.TRAINING_WINDOW_SIZE, 0, -1):
			dataframe['t-' + str(i)] = self.series.shift(i)

		dataframe['t'] = self.series.values
		dataframe = dataframe[self.TRAINING_WINDOW_SIZE:]

		ratio = dataframe[:training_set_end_date].shape[0] / dataframe.shape[0]
		data_set = dataframe.values
		# split into training and test set
		self.training_set_size = int(len(data_set) * ratio)
		self.test_set_size = len(data_set) - self.training_set_size
		train, test = data_set[0: self.training_set_size], data_set[self.training_set_size: len(data_set)]

		# split into input and labels
		self.X_train = train[:, 0 : -1]
		self.Y_train = train[:, -1]
		self.X_test = test[:, 0 : -1]
		self.Y_test = test[:, -1]

		self.model = RandomForestRegressor(n_estimators=500, random_state=1)
		self.model.fit(self.X_train, self.Y_train)

		print("trainning finished for attraction: " + str(self.attraction_number))


	def predict(self, prediction_no):
		self.predictions = list()

		for t in range(prediction_no):
			y_hat = self.model.predict([self.X_test[t]])
			self.predictions.append(y_hat[0])
			# obs = self.Y_test[t]
			# print('predicted=%f, expected=%f' % (y_hat, obs))


	def predict_look_ahead(self, prediction_no, starting_date='2018-05-26 10:05:00'):
		self.prediction_no = prediction_no
		print("predict ahead started for attraction: " + str(self.attraction_number))
		self.predictions = list()
		X_test_look_ahead = []
		X_test_look_ahead.append(self.X_train[-1])

		for t in range(prediction_no):
			y_hat = self.model.predict([X_test_look_ahead[t]])
			self.predictions.append(y_hat[0])
			next_input = np.append(X_test_look_ahead[-1][1:], (y_hat[0]))
			X_test_look_ahead.append(next_input)
			# obs = self.Y_test[t]
			# print('predicted=%f, expected=%f' % (y_hat, obs))

		print("predict ahead finished for attraction: " + str(self.attraction_number))
		return self.predictions


	def display_results(self):
		self.Y_test = np.array(self.Y_test)
		self.predictions = np.array(self.predictions)

		if MAKE_SERIES_STATIONARY_DIFF:
			# map the data to the initial scale
			self.Y_test = np.insert(self.Y_test, 0, self.series_orig[self.training_set_size + self.TRAINING_WINDOW_SIZE + 1])
			self.Y_test = self.Y_test.cumsum()
			self.Y_test = self.Y_test[1:]
			self.predictions = np.insert(self.predictions, 0, self.series_orig[self.training_set_size + self.TRAINING_WINDOW_SIZE + 1])
			self.predictions = self.predictions.cumsum()
			self.predictions = self.predictions[1:]
			self.data_set_init = Series.append(Series([self.init_state]), self.series)
			self.data_set_init = self.data_set_init.cumsum()
			self.data_set_init = self.data_set_init[1:]

		if MAKE_SERIES_STATIONARY_LOG:
			self.Y_test = np.exp(self.Y_test)
			self.predictions = np.exp(self.predictions)
			self.data_set_init = np.exp(self.data_set_init)

		error = mean_squared_error(self.Y_test[:self.prediction_no], self.predictions)
		print('Test MSE: %.3f' % error)

		self.data_set_init = self.data_set_init[self.TRAINING_WINDOW_SIZE:]

		pyplot.plot(self.data_set_init[0 : self.training_set_size + 1], color='green')
		pyplot.plot(self.data_set_init[self.training_set_size : len(self.data_set_init.values)], color='blue')

		self.predictions = Series(data=self.predictions, index=self.data_set_init[self.training_set_size : self.training_set_size + self.prediction_no].index)
		pyplot.plot(self.predictions, color='red')
		pyplot.show()


if __name__ == '__main__':
	estimator = RandomForestForecast(12)
	estimator.train()
	estimator.predict_look_ahead(int(480/5))
	estimator.display_results()
