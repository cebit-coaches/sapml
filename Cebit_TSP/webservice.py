from flask import Flask
from flask import send_from_directory
from flask import send_file
from flask import request
import os
import uuid
from flask import jsonify
import pandas as pd
from qlearning import q_learning
import sys
import json
import numpy as np

app = Flask(__name__)
static_file_dir = os.path.dirname(os.path.realpath(__file__))

# Get port from environment variable or choose 9099 as local default
port = int(os.getenv("PORT", 8080))

@app.route('/find', methods=['POST'])
def find():
	if request.files['file_walking_rewards'] == None:
		return 400, "No file with walking rewards passed"

	filename_walking_rewards = str(uuid.uuid4()) + '.csv'
	f = request.files['file_walking_rewards']
	f.save(filename_walking_rewards)
	
	if request.files['file_waiting_rewards'] == None:
		return 400, "No file with waiting rewards passed"

	filename_waiting_rewards = str(uuid.uuid4()) + '.csv'
	f = request.files['file_waiting_rewards']
	f.save(filename_waiting_rewards)

	attractions_number = int(request.form.get('attractionsNumber', 19))
	park_opening_minutes = int(request.form.get('parkOpeningMinutes', 480))
	sensors_minutes_step = int(request.form.get('sensorsMinuteStep', 5))
	start_state = int(request.form.get('startState', 0))
	end_state = int(request.form.get('endState', 20))

	rewards = pd.read_csv(filename_walking_rewards, header=None)
	rewards = abs(rewards) * -1

	with open(filename_waiting_rewards) as f:
		waiting_rewards = json.load(f)
		# print(waiting_rewards)
		waiting_rewards = np.asarray(waiting_rewards)
		waiting_rewards = abs(waiting_rewards) * -1

	attractions_throughput = pd.read_csv(static_file_dir + '/' + "attractions_throughput.csv", header=None)
	attractions_throughput = attractions_throughput[0]

	state_sequence = q_learning(
            R=rewards,
            R_waiting=waiting_rewards,
            attractions_throughput=attractions_throughput,
            time_step=sensors_minutes_step,
            max_time=park_opening_minutes,
            start_state=start_state,
            end_state=end_state)

	os.unlink(filename_walking_rewards)
	os.unlink(filename_waiting_rewards)

	return jsonify(state_sequence)

if __name__ == '__main__':
	# Run the app, listening on all IPs with our chosen port number
	app.run(host='0.0.0.0', port=port)