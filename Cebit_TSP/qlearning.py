import numpy as np
import random
import sys

# Reward for entering an invalid state.
# There is an if statement which makes sure that an invalid state is never entered.
# It should be the smallest reward in the system otherwise equation max(Q[next_state, :]) breaks
INVALID_STATE_REWARD = 0

# Users may want to come back to the same fun-park ride, however, it is not fun so some penalty should be set for this case
RETURN_TO_SAME_STATE_REWARD = 0

ALLOW_MULTIPLE_STATE_SELECTION = False


def roundToBase(value, base=5):
    value = value / 5
    return int(value)

def q_learning(
        R,
        R_waiting,
        attractions_throughput,
        time_step,
        max_time,
        start_state,
        end_state,
        episodes=1000,
        alpha=0.1,
        gamma=0.9,
        epsilon=0.2,
        visit_all_states=True,
        epsilon_decay=0.01,
        current_minute=0):
    """
    Q-Learning
    
    As in section 6.5 of Reinforcement Learning: An Introduction

    :param R: Rewards table.
    :param start_state: Initial state.
    :param end_state: Final state.
    :param episodes: Number of episodes.
    :param alpha: Step size
    :param gamma: Discount factor
    :param epsilon: Threshold for picking greedy (instead of random) action
    :param visit_all_states: Boolean indicating if all states must be visited
    :param epsilon_decay: Decay factor for epsilon parameter
    :return: Q matrix (same format as rewards)
    """

    Q = np.zeros_like(R, dtype=float)
    c = 0
    for i in range(episodes):
        s = start_state

        num_of_rewards_in_episode = 0
        R_copy = np.copy(R)
        state_sequence = [start_state]
        is_final = False
        current_minute = 0

        while not is_final:
            a = take_epsilon_greedy_action(Q, R_copy, s, epsilon, end_state)
            walking_time_minutes = int(R_copy[s, a])

            if a == 0 or a == R.shape[0]-1:
                delta_q = 0
            else:
                waiting_time_index = roundToBase(abs(walking_time_minutes) + current_minute)

                if waiting_time_index >= int(max_time / time_step):
                    waiting_time_index = int(max_time / time_step) - 1

                users_in_queue = R_waiting[a-1][waiting_time_index] # -1 because 0 state is the entrance and waiting time is applicable only to attractions
                delta_q = (users_in_queue / attractions_throughput[a-1]) * 60
                            
            reward = R_copy[s, a] - delta_q
            current_minute += delta_q + abs(walking_time_minutes)
           
            if a == end_state or current_minute >= max_time: # when end state is reached or the fun-park closes
                if len(state_sequence) < (R.shape[0] - len(np.where(~R.any(axis=0))[0]) - 1) and visit_all_states:
                    # punishes leaving before visiting all states
                    reward *= 10**6
                is_final = True

            state_sequence.append(int(a))
            # just for the sake of clarity
            next_state = a

            # mask invalid states for next max transition action
            blocked_states = R_copy[next_state, :] == INVALID_STATE_REWARD
            transitions = np.ma.array(Q[next_state, :], mask=blocked_states)
            max_transition = np.max(transitions)
            if not max_transition:
                max_transition = 0

            Q[s, a] = Q[s, a] + alpha * (reward + gamma * max_transition - Q[s, a])
            ## or
            # Q[s, a] = (1 - alpha) * Q[s, a] + alpha * (reward + gamma * max_transition) 

            R_copy[:, s] = np.full(R.shape[1], INVALID_STATE_REWARD)
            
            num_of_rewards_in_episode += R_copy[s, a] - delta_q
            s = next_state
            epsilon -= epsilon * epsilon_decay


    print("Episode " + str(i))
    print("Rewards " + str(num_of_rewards_in_episode))
    print("State sequence " + str(state_sequence))
    print("Epsilon " + str({epsilon}))
    print("Q matrix")
    print(Q)
    return state_sequence


def take_epsilon_greedy_action(Q, R, current_state, epsilon, end_state):
    """
    Takes epsilon greedy action
    
    :param Q: Q matrix
    :param R: rewards matrix
    :param current_state: Current state
    :param epsilon: Threshold for picking greedy (instead of random) action
    :param end_state: Final state
    :return: Action (next state)
    """

    r = random.random()
    blocked_states = R[current_state] == INVALID_STATE_REWARD
    if r > epsilon: # choose action greedily
        transitions = np.ma.array(Q[current_state], mask=blocked_states)
        action = np.argmax(transitions)
    else: # choose action randomly
        actions = np.array(range(Q.shape[0]))
        valid_actions = actions[blocked_states == False]
        if len(valid_actions) == 0:
            action = end_state
        else:
            action = random.choice(valid_actions)
    return action

